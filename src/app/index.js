import React from "react";
import "./index.scss";
import {Button} from "reactstrap";

export default () => {
    return (
        <div>
            <Button color="primary">
                Sample Button
            </Button>
        </div>
    )
}
