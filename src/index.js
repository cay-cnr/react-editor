import React from "react";
import ReactDom from "react-dom";
import "bootstrap/scss/bootstrap.scss";

import App from "./app/";

const Index = () => {
    return (
        <React.Fragment>
            <App/>
        </React.Fragment>
    )
};

ReactDom.render(<Index/>, document.getElementById("root"));

